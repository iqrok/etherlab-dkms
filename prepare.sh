#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_DIR=${DIR}/scripts

if [ "$EUID" -ne 0 ]; then
	echo "Please run with sudo"
	exit 1
fi

DISTRO=$(lsb_release -i | awk '{print $3}')
KERNEL_VERSION=$(uname -r | grep -Po "[0-9]+\.[0-9]+")

echo "Current Distro ${DISTRO} ${KERNEL_VERSION}"
echo "------------------------------------------"

if [[ ${KERNEL_VERSION} != "4.19" ]]; then
	echo "GRUB_DISABLE_SUBMENU=y" >> /etc/default/grub
	if [[ ${DISTRO^^} == "UBUNTU" ]]; then
		pushd /tmp
		wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v4.19.195/amd64/linux-headers-4.19.195-0419195-lowlatency_4.19.195-0419195.202106160639_amd64.deb
		wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v4.19.195/amd64/linux-headers-4.19.195-0419195_4.19.195-0419195.202106160639_all.deb
		wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v4.19.195/amd64/linux-image-unsigned-4.19.195-0419195-lowlatency_4.19.195-0419195.202106160639_amd64.deb
		wget -c https://kernel.ubuntu.com/~kernel-ppa/mainline/v4.19.195/amd64/linux-modules-4.19.195-0419195-lowlatency_4.19.195-0419195.202106160639_amd64.deb
		dpkg -i *.deb
		popd

		apt remove -y linux-image-$(uname -r) linux-modules-extra-$(uname -r) linux-headers-$(uname -r) linux-modules-$(uname -r)

		_NOT_419=$(ls /boot/vmlinuz-* -l | grep -Po "[\d]+\.[\d]+.[\d]+\-[\d]+\-[a-zA-Z0-9]+" | grep -v "4.19")
		if [[ ! -z ${_NOT_419} ]]; then
			apt remove -y linux-image-${_NOT_419} linux-modules-extra-${_NOT_419} linux-headers-${_NOT_419} linux-modules-${_NOT_419}
		fi

		apt autoremove -y

		update-grub

		echo "Need to reboot to apply new kernel. On the next boot, run this script once again"
		read -p "Press any key to reboot..." _tmp
		reboot
		exit 0
	fi
else
	echo "Installing required tools..."
	apt update
	apt install -y build-essential libtool automake tree dkms git mercurial

	read -t 10 -p "Install CCAT driver?[y/N] " _installCCAT
	if [[ ${_installCCAT^^} == "Y" ]]; then
		${SCRIPT_DIR}/10-CCAT-env.sh
	fi

	${SCRIPT_DIR}/10-setup-etherlab-dkms.sh
fi
