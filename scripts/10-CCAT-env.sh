#!/bin/bash

if [ "$EUID" -ne 0 ]; then
	echo "Please run as root"
	exit 1
fi

CCAT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CCAT_DIR=${CCAT_DIR}/../CCAT-env

pushd ${CCAT_DIR}

make init
make dkms_add
make dkms_build
make dkms_install
make setup

popd
