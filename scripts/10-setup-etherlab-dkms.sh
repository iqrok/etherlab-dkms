#!/bin/bash

if [ "$EUID" -ne 0 ]; then
	echo "Please run as root"
	exit 1
fi

ETHERLAB_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ETHERLAB_DIR=${ETHERLAB_DIR}/../etherlabmaster

pushd ${ETHERLAB_DIR}

echo "================================== LIST ETHERNET PORTS =================================="
ip a
echo "================================== =================== =================================="
read -p "Select ethernet port to be assigned as ethercat master [default eth0] : " _ethernetPort

if [[ -z ${_ethernetPort} ]]; then
	_ethernetPort="eth0"
fi

echo "ETHERCAT_MASTER0=${_ethernetPort}" > ethercatmaster.local

make showopts

read -p "Press any key to continue..." _tmp

make build
make install

make dkms_add
make dkms_build
make dkms_install
make setup

systemctl start ethercat
ethercat master

popd
